// Definición de la función "burbuja" que ordena una lista de números intercambiando posiciones dentro de dos cíclos: interno y externo
#include <stdio.h>
void bubble_sort(int a[], int n) {
    int i = 0, j = 0, tmp;
    for (i = 0; i < n; i++) {   // ejecución del loop "n" veces (1 por uno o ciclo externo)
        for (j = 0; j < n - i - 1; j++) { // elemento "i" esimo ordenado
            if (a[j] > a[j + 1]) {  // intercambio de comparaciones o pivote (1 por uno o ciclo interno)
                tmp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = tmp;// registro o elemento ordenado temporalmente
            }
        }
    }
}
int main() {
  int a[100], n, i, d, swap; // variables locales
  printf("Ingresa el número de elementos que la lista alojará para ordenar:\n");
  scanf("%d", &n); 
  printf("Ingresa %d números enteros\n", n);
  for (i = 0; i < n; i++)
    scanf("%d", &a[i]);
  bubble_sort(a, n); // invocación de la función "bubble_sort" y sus parámetros
  printf("La lista ordenada es\n");
  for (i = 0; i < n; i++)
     printf("%d\n", a[i]);
  return 0;
}