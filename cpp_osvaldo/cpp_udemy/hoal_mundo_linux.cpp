// Code name: Hola mundo desde Lnux
// Name: oz
// Source: udemy

#include <iostream> // Librería estandar de ntrada y salida de datos.

using namespace std; /* Es un estandar de C++ que sirve para invocar solamente "cout<<" cada vez que se necesite imprimir algo,
                        en lugar de "std::cout<<"...*/ 

int main() { // función principal y dentro de ella se escribe todas las acciones a realizar.

    cout<<"Hola mundo desde Linux"<<endl; // endl es equivalente a la función de escape \n


    return 0; //  por el momento detectará que el programa finalizó correctamente.
}

