// Code name: Operaciones
// Name: oz
// Source: udemy

/* Realizar un programa que; lea de la entrada estándar dos números y
muestre en la salida estándar su suma, resta, multiplicacioń y división. */

#include <iostream> 

using namespace std; 


// La declaración de variables puede ser globla. Esto quiere decir que previamente están definidas para todo el script.

double n1, n2, suma=0,resta=0,multiplicacion=0,division=0; // Declaración de variables globales dobles.

int main() { 

    cout<<"\n******** Realizarémos las operaciones básicas ingresando 2 números enteros ********"<<endl;

    cout<<"\nDigita el primer número: ";cin>>n1;// el caracter de escape "\n" sirve para agregar un salto de línea

    cout<<"\nDigita el segundo número: ";cin>>n2;

    // operaciones

    suma = n1 + n2;
    resta = n1 - n2;
    multiplicacion = n1 * n2;
    division = n1 / n2;

    // impresión de los resultado

    cout<<"\n      Los resutados de las operaciones básicas son: "<<endl; 
    cout<<"\n*La suma de los dos números es igual a: "<<suma<<endl;
    cout<<"*La resta de los dos números es igual a: "<<resta<<endl;
    cout<<"*La multiplicación de los dos números es igual a: "<<multiplicacion<<endl;
    cout<<"*La división de los dos números es igual a: "<<division<<endl;

  
    return 0; 
}

