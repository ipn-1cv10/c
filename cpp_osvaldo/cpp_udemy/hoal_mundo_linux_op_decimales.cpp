// Code name: Operaciones con decimales
// Name: oz
// Source: udemy

/* Realizar un programa que lea 2 números para la expresión (a+b) + 1: */

#include <conio.h>
#include <stdio.h>

using namespace std; 


// Declaración de variables globales.

float a,b, resultado=0;

int main() { 

    clrscr();
    cout<<"\n******** Ingresa los valores solicitados para la expresión (a/b) + 1 ********"<<endl;

    cout<<"\nIngrese el valor para a: ";cin>>a;

    cout<<"\nIngrese el valor para b: ";cin>>b;

    resultado = a/b + 1; // C realiza las operaciones de acuerdo a su jerarquía.

    cout<<"\nEl resultado es: "<<resultado<<endl;

    return 0; 
}

