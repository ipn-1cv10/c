// Code name: Tipos de datos
// Name: oz
// Source: udemy

#include <iostream> // Librería estandar de ntrada y salida de datos.

using namespace std; 

// Una variable es un espacio en memoria de nuestros esquipos

int main() { 

    int entero = 14; // con int podemos uilizar variables de tipo entero
    float flotante = 10.45; // con float podemos utilizar decimales
    double mayor = 12.89765783; // con double podemos utilizar números más grandes
    char cadena = 'a'; // podemos guardar caracteres

    cout<<entero<<endl;
    cout<<mayor<<endl;
    cout<<flotante<<endl;
    cout<<cadena<<endl;

    return 0; 
}

