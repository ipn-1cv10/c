// Code name: Entrada de datos
// Name: oz
// Source: udemy

/* Realizar un programa que lea de la entrada estándar los siguientes datos de una personas:

1.- Edad: con un dato de tipo entero
2.- Sexo: con un dato de tipo caracter
3.- Altura en metros: con un dato de tipo real

Tras leer los datos, necesitamos imprimirlos desde la salida estándar. */

#include <iostream> 

using namespace std; 


// Declaración de variables globales.

int edad;
char sexo[10];
float altura;

int main() { 

    cout<<"\n******** Ingresa los datos que se solicita de una persona********"<<endl;

    cout<<"\n¿Cuál es su edad?: ";cin>>edad;// el caracter de escape "\n" sirve para agregar un salto de línea

    cout<<"\n¿Cuál es su sexo?: ";cin>>sexo;

    cout<<"\n¿Cuál es su altura en metros?: ";cin>>altura;

    // impresión de los datos

    cout<<"\nEdad: "<<edad<<endl;
    cout<<"\nSexo: "<<sexo<<endl;
    cout<<"\nAltura: "<<altura<<endl;

    return 0; 
}

