/****************HOLA MUNDO DESDE LINUX Y MÁS***************************/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>





int main(void){
	
	/*Bloque de secuencias de escape*/
	printf("\n");
	printf("* Hola mundo desde Linux *\n--------------------------\n\n\n\t");
	printf("En esta ocasión revisaremos aspectos importantes del lenguaje de programación C/C++.\n\t");
	printf("Específicamente nos referímos a los típos de datos, las secuencias de escape, caracteres de contro\n\t");
	printf("entre otras cosas más.\n\n\n");
	printf("SECUENCIAS DE ESCAPE\n--------------------\n\n\t");
	printf("Muchos lenguajes de programación admiten un concepto llamado Secuencia de escape. Cuando un carácter está precedido por una barra invertida\n\t");
	printf("'back slash' (\\) se llama secuencia de escape y tiene un significado especial para el compilador.\n\n\t");
	printf("Existen varias secuencias de escape. A continuación, mencionaremos las más relevantes acompañadas de su ejemplo:\n\n\n\t");

	printf("(\\n)\n\t----\n\n\t");
	printf("Cuando insertas '\\n'\n\tse agrega una nueva línea\n\tde texto.\n\n\n\n\t");

	printf("(\\t)\n\t----\n\n\t");
	printf("Cuando insertas '\\t' \tse agrega un tab \t(espacios) en una línea\tde texto.\n\n\n\n\t");

	printf("(\\b)\n\t----\n\n\t");
	printf("Cuando \bse incerta '\\b' (antes de una palabra) se \beliminan los espacios en \b \buna cadena\b de texto\b\n\n\n\n\t");
	
	printf("(\\r)\n\t----\n\n\t");
	printf("Esto sucede cuando insertas '\\r':\n\t");
	printf("Texto antes de\r\tel texto se cortó al encontrarse con una secuencia de escape '\\r'.\n\n\n\t");

	printf("(\\f)\n\t----\n\n\t");
	printf("Esto sucede \fcuando se incerta '\\f' repetidas veces \f en una cadena \f de texto.\n\n\n\n\t");

	printf("(\\')\n\t----\n\n\t");
	printf("Cuando se desea agregar comillas simples (p.eg) \'palabra\', esta secuencia se inserta antes y al final de lo que se desea entrecomillar.\n\n\n\n\t");

	printf("(\\\")\n\t----\n\n\t");
	printf("Cuando se desea agregar comillas dobles (p.eg) \"palabra\", esta secuencia se inserta antes y al final de lo que se desea entrecomillar.\n\n\n\n\t");

	printf("(\\\\)\n\t----\n\n\t");
	printf("Insertar \"back slashe's' (\\\\) nos permite saber; con qué secuencia estamos trabajando.\n\n\n\n\n\n");

	printf("TIPOS DE DATOS NUMERICOS\n-------------------------------------\n");
	printf ("En programacion, un tipo de dato informatico o simplemente tipo es un atributo de los datos que indica al ordenador (y/o al programador) sobre la clase de datos que se va a trabajar. Esto incluye imponer restricciones en los datos, como que valores pueden tomar y que operaciones se pueden realiza");
	printf("\n\n");
	printf("\n\n");
	printf("En una variable tipo char los numeros van de -128 a 127 o 0 a 255 en enteros de un byte");
	printf("\n\n");
	printf("En una variable tipo int los numeros van de -32,768 a 32,767 o -2,147,483,648 a 2,147,483,647 en enteros de 4 bytes ");
	printf("\n\n");
	printf("En una variable tipo short los numeros van de -32,768 a 32,767 en enteros de 2 bytes ");
	printf("\n\n");
	printf("En una variable tipo long los numeros van de -9223372036854775808 a 9223372036854775807 en enteros de 8 bytes ");
	printf("\n\n");
	printf("En una variable tipo float los numeros van de 3.e +/-38 en enteros de 4 bytes ");
	printf("\n\n");
	printf("En una variable tipo double los numeros van de 1.7e +/-308 en enteros de 8 bytes ");
	printf("\n\n");
	printf("\n\n");
	
	printf("USO DEL SCANF\n---------------------------\n");
	printf("En C, la funcion scanf, en realidad representa a una familia de funciones que analizan una entrada de datos con formato y cargan el resultado en los argumentos que se pasan por referencia a dicha funcion o funciones");
	printf("\n\n");
	printf("\n\n");

	char str1[20], str2[30];

   	printf("Enter name: ");
   	scanf("%19s", str1);

   	printf("Enter your website name: ");
   	scanf("%29s", str2);

   	printf("Entered Name: %s\n", str1);
   	printf("Entered Website:%s", str2);
	
	int a = 10;
	float b = 3.14;
	char ch = '5';
	
	printf("%d\n", a);     //Integer format specifier
	printf("%f\n", b);     //Float format specifier
	printf("%c\n", ch);      //Character format specifier

	return 0;

}