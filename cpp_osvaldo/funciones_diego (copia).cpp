/**
*Nombre: Osvaldo Álvarez García
*Secuencia:1CV10
*Programa: Función Burbuja (v2.1)
*/

//Librerias
#include <stdio.h> 
#include <stdlib.h> 
#include <conio.h>
#define MAX 100
#include <windows.h> 
#include <ctype.h> 
#define color SetConsoleTextAttribute

//FUNCIONES
void MAYORDE3NUMEROS(void);
void fBurbuja(void);

//VARIABLES GLOB.
int total;
int vNumeros[MAX]; 
int j, i, temp;

int vopcion = 0;
int num;
char letra (letra);
char cadena[50] = "";
char str1[50], str2[60], str3[4], str4[5];
char cad[]="El valor de";
float c=3.1416;
int a=20, b=10;
char ch = '5';


//Se enlistan los casos
int main()
{
 do
   {
        HANDLE hConsole = GetStdHandle( STD_OUTPUT_HANDLE );
		system("cls"); 
		color(hConsole,31); /*Esta linea es para agregarle  un color que esta Parpadeando a lo que se imprima */
		printf("\n\n                                                MENU PRINCIPAL:                                                        \n");
		color(hConsole,11);	        
		printf("\n\n     OPCION 1 = HOLA MUNDO. \n________________________________");
		printf("\n");
		printf("\n\n     OPCION 2 = INVESTIGACION DEL USO DE CADENA EN C++\n____________________________________________________________________________");
		printf("\n");
		printf("\n\n     OPCION 3 = INVESTIGACION DE LAS SECUENCIAS DE ESCAPE Y TIPOS DE DATOS.\n__________________________________________________________________________ ");
		printf("\n");
		printf("\n\n     OPCION 4 = MAYOR DE 3 NUMEROS.\n__________________________________________________________________________ ");
		printf("\n");
		printf("\n\n     OPCION 5 = BURBUJA.\n__________________________________________________________________________ ");
		printf("\n");
		printf("\n\n     OPCION 6 = SALIR DEL PROGRAMA.\n_______________________________________");
		color(hConsole,15);
		printf("\n\n     DIGITE UNA OPCION: \n__________________________\n\t");
		printf("\n");
	    scanf("%i",&vopcion);
	    system("cls");
	    printf("\n\n");
	    color(hConsole,12);
	    printf("\tPara lograr identificar la opci\xA2n que est\xA0s tecleando, introduce cualquier caracter\n");
	    printf("\n\n");
	    color(hConsole,15);
	    printf("\tPara continuar digita cualquier caracter:");
	    scanf("%s", &letra);
	    fflush(stdin);
	       if (isalpha(letra)) 
		    {
              color(hConsole,159);
			  printf("\n\nEl caracter que tecleaste es un caracter tipo letra");
			  color(hConsole,3);
			  printf("\n_____________________________________________________");
			  printf("\n\n");
			  printf("\n\nAhora para ver la opci\xA2n que seleccionaste del Men\xA3 presiona 'ENTER'");
	        }
			 else 
			{
	          color(hConsole,159);
			  printf("\n\nEl caracter que tecleaste es un caracter tipo numerico");
			  color(hConsole,3);
			  printf("\n_______________________________________________________");
			  printf("\n\n");
			  printf("\n\nAhora para ver la opci\xA2n que seleccionaste del Men\xA3 presiona 'ENTER'");
	        } getch();
		switch (vopcion)      				
		{
			case 1:
				{
				
					system("cls"); 
					color(hConsole,3);
					printf("\n\n HOLA MUNDO \n");
					color(hConsole,15);
					printf("\n");
					color(hConsole,12);
	                printf("\t%c",168);/* Esta funcion ("%c",168) es para poner este signo de interrogacion = "¿" */
					printf("Quieres continuar en el programa o no\?",num); 
					printf("\n\n");
					color(hConsole,15);
	                printf("\t1 = Si \n\n ",num); 
	                printf("\n");
	                printf("\t2 = No \n\n ");
	                scanf("%i", & num);
	                	if (num == 1)
	                      {
	                        return main ();
	                      }
	
	                    else
	
	                     {
	                        return 0;
					     }
	            
					getch();
				}
			   break;
				    
			case 2:	
			    {
					system("cls");
					color(hConsole,3);
					printf("\n\n INVESTIGACION DEL USO DE CADENAS DE CARACTERES EN C++:\n______________________________________________________________");
					scanf("%49[^\n]",&cadena); //lee cualquier excepcion del salto de linea scanf ("%49[aeiou]",&cadena); lee solo laas vocales
					printf("\n\nCadena de caracteres.\n______________________________");
					color(hConsole,15);
					printf("\n\nUna cadena en C++ es un conjunto de caracteres, o valores de tipo 'char', terminados con el carácter nulo. Internamente se almacenan en posiciones consecutivas de memoria en forma de arreglo..\n\n");
		            printf("\n\n");
		            color(hConsole,12);
	                printf("\t%c",168);/* Esta funcion ("%c",168) es para poner este signo de interrogacion = "¿" */
					printf("Quieres continuar en el programa o no\?",num); 
			        color(hConsole,15);
					printf("\n\n");
	                printf("\t1 = Si \n\n ",num); 
	                printf("\n");
	                printf("\t2 = No \n\n ");
	                scanf("%i", & num);
	                	if (num == 1)
	                      {
	                        return main ();
	                      }
	
	                    else
	
	                     {
	                        return 0;
					     }
	            
					getch();
	
				}   
				break;
				    
			case 3:
				{
			   	system("cls");
			   	color(hConsole,3);
			   	system("cls");
			   	printf("En esta tarea se mostraran dos aspectos importantes en el lenguaje de programacion C++\n\n");
		        printf("Principalmente trataremos con los tipos de datos y las secuencias de escape\n\n\n");
		      
		        printf("DEFINICION DE PRINTF:\n_______________________\n\n");
		        color(hConsole,15);
		        printf("La funcion 'printf' transporta datos desde la memoria a la pantalla, a diferencia de scanf, que envia datos desde el teclado para almacenarlos en la memoria. La funcion printf devuelve el numero de caracteres escritos. Si devuelve un valor negativo indica que se ha producido un error.\n\n\n");
		      
		        color(hConsole,3);
		        printf("SECUENCIAS DE ESCAPE\n___________________________\n\n");
		        color(hConsole,15);
		        printf("Estas se emplean para proporcionar representaciones literales de caracteres no imprimibles y de caracteres que normalmente tienen significados especiales:\n\n");
		        printf("Para continuar con el tema, les presentare algunas de las secuencias de escape:\n\n");
		      
		        color(hConsole,3);
		        printf("'back slash' (\\) o en espanol 'diagonal invertida' es una secuencia de escape y tiene un significado especial para el compilador, en este caso siendo un salto de linea.\n\n");
		        printf("EJEMPLO:\n");
		        color(hConsole,15);
			    printf("Hola\n");
		        printf("Mundo\n\n");
		      
		        color(hConsole,3);
		        printf("Cuando insertas '\\t' se agrega un espacio en una linea de texto.\n\n");
		        printf("EJEMPLO:\n");
		        color(hConsole,15);
			    printf("Hola\tMundo\n\n");
		      
		       color(hConsole,3);
		        printf("Cuando insertas '\\b' (antes de una palabra) se reduce o elimina el espacio entre palabras.\n\n");
		        printf("EJEMPLO:\n");
		        color(hConsole,15);
			    printf("Hola\bMundo\n\n");
			  
			    color(hConsole,3);
		        printf("Cuando insertas '\\r' (antes de una palabra) este elimina el texto anterior a la secuencia de escape '\\r'.\n\n");
		        printf("EJEMPLO:\n");
		        color(hConsole,15);
			    printf("Hola\rMundo\n\n");
			  
                color(hConsole,3);			  
			    printf("Cuando insertas '\\f' estas indicando que estas avanzando de pagina.\n\n");
		        printf("EJEMPLO:\n");
		        color(hConsole,15);
			    printf("Hola Mundo\f\n\n");
		
		        color(hConsole,3);
			    printf("Cuando insertas '\\a' se usa para dar una alerta o advertencia en forma de sonido en tu equipo.\n\n");
		        printf("EJEMPLO:\n");
		        color(hConsole,15);
			    printf("Hola\aMundo\n\n");
			  
			    color(hConsole,3);
			    printf("Cuando se require poner comillas simples se usa \\' .\n\n");
		        printf("EJEMPLO:\n");
		        color(hConsole,15);
			    printf("Hola\'Mundo\'\n\n");
			   
			    color(hConsole,3);
			    printf("Cuando se require poner comillas dobles se usa \\\" .\n\n");
		        printf("EJEMPLO:\n");
		        color(hConsole,15);
			    printf("Hola\"Mundo\"\n\n");
			  
			    color(hConsole,3);
		        printf("Insertar \"back slashe's' \\\\ nos permite saber, con que secuencia estamos trabajando.\n\n");
		        printf("EJEMPLO:\n");
		        color(hConsole,15);
			    printf("Hola\\Mundo\n\n");
			  
			    color(hConsole,3);
		        printf("Cuando metes signo de interrogacion literal '\\?' .\n\n");
		        printf("EJEMPLO:\n");
		        color(hConsole,15);
			    printf("Hola Mundo\?\n\n\n\n");
			  
			    color(hConsole,3);
			    printf("TIPOS DE DATOS\n___________________________\n\n");
			    color(hConsole,15);
		        printf("Los tipos de datos basicos normalmente son los numericos y en seguida veremos tipos de datos simples en C++\n");
		        printf("Para continuar con el tema, les presentare algunas de los tipos de datos:\n\n");
		       
			    printf("En una variable tipo 'char' los numeros enteros van de -128 a 127 o 0 a 255 sin signo en entero de 1 byte.\n\n");
			    printf("En una variable tipo 'short' los numeros enteros van de -32,768 a 32,767 en entero de 2 bytes.\n\n");
			    printf("En una variable tipo 'int' los numeros enteros van de -32,768 a 32,767 en entero de 4 bytes.\n\n");
			    printf("En una variable tipo 'long' los numeros enteros van de -9223372036854775808 a 9223372036854775807 en entero de 8 bytes.\n\n");
			    printf("En una variable tipo 'float' los numeros enteros van de 3.4e +/- 38(7 digitos) en entero de 4 bytes.\n\n");
			    printf("En una variable tipo 'double' los numeros enteros van de 1.7e +/- 308(15 digitos) en entero de 8 bytes.\n\n\n\n\n");
			   
			    color(hConsole,3);
			    printf("USO DE SCANF\n___________________\n\n");
			    printf("Mediante la funcion scanf podemos introducir cualquier combinacion de valores numericos, caracteres sueltos y cadenas de caracteres a traves del teclado.\n\n");
		 	    printf("EJEMPLO 1:\n\n");
		 	    color(hConsole,15);
		        printf("Introduce tu nombre: ");
		        scanf("%19s", str1);
		 
		        printf("Introduce tu correo electronico: ");
		        scanf("%29s", str2);
		 
		        printf("Nombre registrado: %s\n", str1);
		        printf("Correo registrado:%s\n\n", str2);
		        color(hConsole,3);
		        printf("EJEMPLO 2:\n\n");
		        color(hConsole,15);
		        printf("%d\n", a);     //Integer format specifier
		        printf("%f\n", b);     //Float format specifier
		        printf("%c\n", ch);      //Character format specifier
		        system("pause");
		        color(hConsole,207);
	            printf("\t%c",168);/* Esta funcion ("%c",168) es para poner este signo de interrogacion = "¿" */
				printf("Quieres continuar en el programa o no\?",num); 
			    color(hConsole,15);
				printf("\n\n");
	            printf("\t1 = Si \n\n ",num); 
	            printf("\n");
	            printf("\t2 = No \n\n ");
	            scanf("%i", & num);
	                	if (num == 1)
	                      {
	                        return main ();
	                      }
	
	                    else
	
	                     {
	                        return 0;
					     }
	            
					getch();
				}	
				break;
			case 4:
				{
				system("cls");
				color(hConsole,13);
				printf("\n\n  INTRODUCE TRES NUMEROS DE TU PREFERENCIA PARA PODER IDENTIFICAR EL MAYOR DE ESTOS\n");
				MAYORDE3NUMEROS();
				return 0;
				}
				break;
			case 5:
				{
                system("cls");
	        	printf ("Cuantos numeros deseas ordenar? "); 
	        	scanf("%d", &total);
	        	fBurbuja();
				return 0;
				}
				break;
			case 6:	
                 {
                system("cls");
                color(hConsole,12);	
				system("cls");
				printf("\t%c",168);/* Esta funcion ("%c",168) es para poner este signo de interrogacion = "¿" */
				printf("ESTAS A PUNTO DE SALIR DEL PROGRAMA,DESEAS SALIR?  SI o NO\n");
				color(hConsole,15);
				printf("\t 1 = NO\n");
				printf("\n");
				printf("\t 2 = SI\n");
				printf(" POR FAVOR DIGITE LA OPCION DE LO QUE QUIERA HACER...\n");
	            scanf("%i", & num);
	                if (num == 1)
	                {
	                 return main ();
	                }
	
	                else
	
	                {
	                    return 0;
					}
	            
					getch();
				 }
				break;
			default:
				{
			    	system("cls");
			    	color(hConsole,11);
					printf("\n\n ERROR (ESTA OPCION NO SE ENCUENTRA EN EL MENU)\n\n\n");
                    color(hConsole,15);
					printf("\n\n PARA REGRESAR AL MENU PRESIONA '1' Y LUEGO 'ENTER':\t");
					scanf("%i", & num);
					     if (num == 1)
	                    {
	                        return main ();
	                    }
	
	                  else
	
	                    {
	                        return 0;
					    }
			          
					getch();
	    
				}    
				break;					
		}

	}  
	while (vopcion !=5);
	system("pause");
	return 0;
}
    void MAYORDE3NUMEROS(void)
    {
    	int numero, mayor;
    	
    	printf( "\n   Introduzca el primer n%cmero (entero): ", 163 );
        scanf( "%d", &mayor );
        printf( "\n   Introduzca el segundo n%cmero (entero): ", 163 );
        scanf( "%d", &numero );
        
        if (numero>mayor)
        mayor = numero;
        
        printf("\n   Introduzca el tercer n%cmero (entero): ", 163);
        scanf( "%d", &numero );
    
        if ( numero > mayor )
        mayor = numero;
        
        printf( "\n   %d es el mayor.", mayor );
	}
	void fBurbuja (void)
	{
    // Lee y almacena los datos en el arreglo 
	        	for (i = 0; i < total; i++) { 
	     		printf ("%d: ", i + 1); 
		    	scanf ("%d", &vNumeros[i]); 
	        	} 
		
		         // Método de búrbuja
	        	for (i = 0; i < (total - 1); i++) { 
		     	for (j = i + 1; j < total; j++) { 
				if (vNumeros[j] < vNumeros[i]) { 
					temp = vNumeros[j]; 
					vNumeros[j] = vNumeros[i]; 
					vNumeros[i] = temp; 
				} 
			} 
		} 
		
		// Números ordenados
		printf ("Los numeros ordenados son:\n"); 
		for (i = 0; i < total; i++) { 
			printf("%d | ", vNumeros[i]); 
		} 
		
		printf("\n"); 
	}